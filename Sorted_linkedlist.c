/*
@Author - Sankalp
Roll Number - 03520902719
Program - WAP TO PERFORM INSERTION, DELETION AND DISPLAY IN SORTED LINKED LISTS.
*/

# include <stdio.h>
# include <stdlib.h>

struct node{
    int data;
    struct node *next;
};


void print(struct node *head){
    struct node *tmp;
    printf("\nPRINTING LINKED LIST\n\n");
    if(head == NULL){
        printf("List is empty.\n");
    }else{
        tmp = head;
        printf("Linked List (");
        while(tmp != NULL){
                printf("%d, ", tmp->data);
                tmp = tmp->next;
        }
        printf(")\n");
    }
}


struct node *make_node(int num){
    struct node *new_node = (struct node*)malloc(sizeof(struct node));
    new_node->data = num;
    new_node->next = NULL;
    return new_node;
}


void sort_insert(struct node **ptr, struct node *to_insert){
    struct node *temp;
    if (*ptr==NULL || ((*ptr)->data)>=to_insert->data){
        to_insert->next = *ptr;
        *ptr = to_insert;
    } else {
        temp = *ptr;
        while (temp->next!=NULL && temp->next->data<=to_insert->data)
            temp = temp->next;    
        to_insert->next = temp->next;
        temp->next = to_insert;
    }
}


void sort_delete(struct node **ptr, int to_delete) {
    if (*ptr == NULL){
        printf("UNDERFLOW");
        return;
    }
    struct node *temp = *ptr;
    if (temp->data == to_delete){
        *ptr = temp->next;
        free(temp);
        return;
    }
    while (temp->next->data < to_delete){
        if (temp->next->next == NULL && temp->next->data<to_delete){
            printf("\nNo element with value %d found!!\n", to_delete);
            return;
        }
        temp = temp->next;
    }
    if (temp->next->data != to_delete){
        printf("\nNo element with value %d found!!\n", to_delete);
        return;
    }
    struct node *new_node = temp->next->next;
    free(temp->next);
    temp->next = new_node;    
}


void menu(){
    printf("\n===== MENU =====\n");
    printf("1. Sorted Insertion\n");
    printf("2. Sorted Deletion\n");
    printf("3. Display Linked List\n");
    printf("4. EXIT\n\n");
}


int main(){
    int choice=0, val=1, num;
    struct node *head = NULL;
    printf("\nTO PERFORM INSERTION, DELETION AND DISPLAY IN SORTED LINKED LISTS.\n\n");
    while (val){
        menu();
        printf("\nYour Choice: (1/2/3/4) ");
        scanf(" %d", &choice);
        switch (choice){
        case 1:
            printf("\nEnter value for insertion in linked list: ");
            scanf("%d", &num);
            struct node *new_node = make_node(num);
            sort_insert(&head, new_node);
            break;
            
        case 2:
            printf("\nEnter value for deletion in linked list: ");
            scanf("%d", &num);
            sort_delete(&head, num);
            break;
            
        case 3:
            print(head);
            break;
            
        case 4:
            val=0;
            break;
            
        default:
            printf("\nINVALID CHOICE !!!\n");
            break;
        }
    }
}