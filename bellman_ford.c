#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

struct Edge
{
    int source, destination, weight;
};
struct Graph
{
    int V, E;
    struct Edge *edge;
};
struct Graph *createGraph(int V, int E)
{
    struct Graph *graph = (struct Graph *)malloc(sizeof(struct Graph));
    graph->V = V;
    graph->E = E;
    graph->edge = (struct Edge *)malloc(graph->E * sizeof(struct Edge));
    return graph;
}
void getSolution(int dist[], int n)
{
    printf("\nVertex\tDistance from Source Vertex\n");
    int i;
    for (i = 0; i < n; ++i)
    {
        printf("%d \t\t\t%d\n", i, dist[i]);
    }
}
void bellmanFord(struct Graph *graph, int source)
{
    int V = graph->V;
    int E = graph->E;
    int distance[V];
    int i, j;
    for (i = 0; i < V; i++)
        distance[i] = INT_MAX;
    distance[source] = 0;
    for (i = 1; i <= V - 1; i++)
    {
        for (j = 0; j < E; j++)
        {
            int u = graph->edge[j].source;
            int v = graph->edge[j].destination;
            int weight = graph->edge[j].weight;
            if (distance[u] + weight < distance[v])
                distance[v] = distance[u] + weight;
        }
    }
    for (i = 0; i < E; i++)
    {
        int u = graph->edge[i].source;
        int v = graph->edge[i].destination;
        int weight = graph->edge[i].weight;
        if (distance[u] + weight < distance[v])
            printf("Negative edge cycle exists!!!!\n");
    }
    getSolution(distance, V);
    return;
}
int main()
{
    int V, E, S;
    printf("Enter no. of vertices in the graph:\t");
    scanf("%d", &V);
    printf("Enter no. of edges in the graph:\t");
    scanf("%d", &E);
    printf("Enter the source of vertex:\t");
    scanf("%d", &S);
    struct Graph *graph = createGraph(V, E);
    int i;
    for (i = 0; i < E; i++)
    {
        printf("\nEnter Edge %d Source, Destination and Weight respectively\t", i + 1);
        scanf("%d", &graph->edge[i].source);
        scanf("%d", &graph->edge[i].destination);
        scanf("%d", &graph->edge[i].weight);
    }
    bellmanFord(graph, S);
    return 0;
}
