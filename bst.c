#include<stdio.h>
#include<stdlib.h>

struct node{
    int data;
    struct node *left;
    struct node *right;
};

void menu(){
    printf("\n=======MENU=======\n");
    printf("1. Insert in BST.\n");
    printf("2. Delete in BST.\n");
    printf("3. INORDER Traversal\n");
    printf("4. POSTORDER Traversal\n");
    printf("5. PREORDER Traversal\n");
    printf("6. EXIT\n");
    printf("Enter Choice (1/2/3/4/5/6): ");
}

void inorder(struct node **root){
    if (*root == NULL)
        return;
    inorder(&((*root)->left));
    printf("%d ", (*root)->data);
    inorder(&((*root)->right));
}

void postorder(struct node **root){
    if (*root == NULL)
        return;
    postorder(&((*root)->left));
    postorder(&((*root)->right));
    printf("%d ", (*root)->data);
}

void preorder(struct node **root){
    if (*root == NULL)
        return;
    printf("%d ", (*root)->data);
    preorder(&((*root)->left));
    preorder(&((*root)->right));
}

struct node *insert(struct node **root, int val){
    if (*root == NULL){
        struct node* new_node = (struct node *)malloc(sizeof(struct node)); 
        new_node->data = val;
        new_node->left  = NULL;
        new_node->right = NULL;
        return new_node;
    }
    if (val<(*root)->data)
        (*root)->left = insert(&((*root)->left), val);
    else if (val>(*root)->data)
        (*root)->right = insert(&((*root)->right), val);
    return *root;
}

struct node *inorder_successor(struct node **root){
    struct node *temp = *root;
    while (temp && temp->left!=NULL)
        temp = temp->left;
    return temp;
}

struct node *delete(struct node **root, int val){
    if (*root == NULL)
        return *root;
    if (val<(*root)->data)
        (*root)->left = delete(&((*root)->left), val);
    else if (val>(*root)->data)
        (*root)->right = delete(&((*root)->right), val);
    else if (val==(*root)->data){
        if ((*root)->left == NULL && (*root)->right == NULL){
            free(*root);
            return NULL;
        }
        else if ((*root)->left == NULL){
            struct node *temp = (*root)->right;
            free(*root);
            return temp;
        }
        else if((*root)->right == NULL){
            struct node *temp = (*root)->left;
            free(*root);
            return temp;
        }
        else{
            struct node *temp = inorder_successor(&((*root)->right));
            (*root)->data = temp->data;
            (*root)->right = delete(&((*root)->right), temp->data);
        }    
    }
    return *root;
}

int main(){
    struct node *root = NULL;
    int choice=1, val;
    while (choice){
        menu();
        scanf("%d",&choice);
        switch (choice)
        {
            case 1:
                printf("\nEnter Value To Insert: ");
                scanf("%d", &val);
                root = insert(&root, val);
                break;
            case 2:
                printf("\nEnter Value To Delete: ");
                scanf("%d", &val);
                root = delete(&root, val);
                break;
            case 3:
                printf("\nINORDER TRAVERSAL IS: ");
                inorder(&root);
                break;
            case 4:
                printf("\nPOSTORDER TRAVERSAL IS: ");
                postorder(&root);
                break;
            case 5:
                printf("\nPREORDER TRAVERSAL IS: ");
                preorder(&root);
                break;
            case 6:
                choice = 0;
                break;
            default:
                printf("INVALID CHOICE\n");
                break;
        }
    }
    return 0;
}