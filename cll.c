#include<stdlib.h>
#include<stdio.h>

struct student {
    char name[50];
    int rollnumber; 
};

struct node {
    struct student st;
    struct node *next;
};

void menu(){
    printf("\n======= MENU =======\n");
    printf("1. Insert a Student at Front\n");
    printf("2. Delete Student at End.\n");
    printf("3. Display Students List.\n");
    printf("4. EXIT\n\n");
    printf("Enter the Choice (1/2/3/4): ");
}

void insert_at_front(struct node **head){
    struct node *new_node = (struct node*)malloc(sizeof(struct node));
    printf("\nEnter Name Of Student (50): ");
    scanf("%s", &(new_node->st.name));
    printf("\nEnter Roll Number: ");
    scanf("%d", &(new_node->st.rollnumber));
    if (*head==NULL){
        *head = new_node;
        new_node->next = *head;
    }
    else{
        struct node *temp = *head;
        while (temp->next != *head){
            temp = temp->next;
        }
        new_node->next = *head;
        temp->next = new_node;
        *head = new_node;
    }
}

void delete_from_end(struct node **head){
    if (*head==NULL)
        printf("No Students Available To Delete.\n");
    else{
        struct node *temp = *head;
        if (temp->next==*head){
            *head = NULL;
            free(temp);
        }
        else{
            struct node *prev;
            while(temp->next!=*head){
                prev = temp;
                temp = temp->next;
            }
            prev->next = *head;
            free(temp);
        }
    }
}

void display_cll(struct node **head){
    if (*head==NULL)
        printf("Student CLL is Empty.\n");
    else{
        printf("\nSTUDENT\tROLL NUMBER\tNAME\n");
        int count=1;
        struct node *temp = *head;
        while (temp->next!=*head){
            printf("%4d\t%5d\t\t%s\n",count, temp->st.rollnumber, temp->st.name);
            temp = temp->next;
            count++;
        }
        printf("%4d\t%5d\t\t%s\n",count, temp->st.rollnumber, temp->st.name);
    }
}

int main(){
    int choice;
    struct node *head = NULL;
    while(choice){
        menu();
        scanf("%d",&choice);
        switch(choice)
        {
            case 1:
                insert_at_front(&head);
                break;
            case 2:
                delete_from_end(&head);
                break;
            case 3:
                display_cll(&head);
                break;
            case 4:
                choice = 0;
                break;
            default:
                printf("Invalid Choice.\n");
                break;
        }
    }
}