#include<stdlib.h>
#include<stdio.h>

struct employee{
    char name[50];
    int id;
};

struct node{
    struct employee e;
    struct node* next;
    struct node* prev;
};

void menu(){
    printf("\n======= MENU =======\n");
    printf("1. INSERT EMPLOYEE AT FRONT.\n");
    printf("2. DELETE EMPLOYEE FROM END.\n");
    printf("3. DISPLAY EMPLOYEE LIST.\n");
    printf("4. EXIT.\n");
    printf("Enter the Choice (1/2/3/4): ");
}

void insert_at_front(struct node **head){
    struct node *new_node = (struct node *)malloc(sizeof(struct node));
    printf("\nEnter Employee Name: ");
    scanf("%s", &(new_node->e.name));
    printf("\nEnter Employee ID: ");
    scanf("%d", &(new_node->e.id));
    new_node->next = *head;
    new_node->prev=NULL;
    if (*head != NULL)
        (*head)->prev = new_node;
    *head = new_node;
}

void delete_at_end(struct node **head){
    if (*head==NULL){
        printf("Employee DLL is Empty.\n");
    }
    else{
        struct node *temp = *head;
        while (temp->next!=NULL){
          temp = temp->next;  
        }
        struct node *prev = temp->prev;
        prev->next = NULL;
        free(temp);
    }
}

void display(struct node **head){
    if (*head==NULL)
        printf("Employee DLL is Empty.\n");
    else{
        struct node* temp = *head;
        int count = 1;
        printf("\nEMPLOYEE\tID\tNAME\n");
        while (temp!=NULL){
            printf("%4d\t%9d\t%s\n", count, temp->e.id, temp->e.name);
            temp = temp->next;
            count++;
        }
    }
}

int main(){
    int choice;
    struct node* head=NULL;
    while (choice){
        menu();
        scanf("%d",&choice);
        switch (choice)
        {
            case 1:
                insert_at_front(&head);
                break;
            case 2:
                delete_at_end(&head);
                break;
            case 3:
                display(&head);
                break;
            case 4:
                choice=0;
                break;
            default:
                printf("INVALID CHOICE!\n");
                break;
        }
    }
    return 0;
}
