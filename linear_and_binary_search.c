/********************************
 @author: SANKALP
 roll no.: 03520902719
 *******************************/
#include <stdio.h>
#include <time.h>

int linear_search(int arr[], int n, int element){
  int i=0;
  for (i=0; i<n; i++){
    if (arr[i]==element)
      return i;
  }
  return -1;
}

int binary_search(int arr[], int n, int element){
  int low = 0, high = n-1, mid;
  while (low<=high){
    mid = (low+high)/2;
    if (arr[mid] == element)
      return mid;
    else if (arr[mid] < element)
      low = mid+1;
    else if (arr[mid] > element)
      high = mid-1;
  }
  return -1;
}

void print_ans(int ans, int search){
  if (ans == -1)
    printf("\n%d => ELEMENT %d NOT FOUND.\n", ans, search);
  else
    printf("\n%d FOUND AT INDEX %d\n", search, ans);
}

int main ()
{
  int choice;
  clock_t t;
  do{
    int i = 0, j = 0, index = 0, search = 0, n = 0, ans=0;
    printf("\nLINEAR SEARCH\n=============\n");
    printf ("\nENTER NUMBER OF ELEMENTS IN ARRAY: ");
    scanf ("%d", &n);
    int ls[n];
    printf ("\nENTER ARRAY ELEMENTS: ");
    for (i = 0; i < n; i++)
    {
      scanf ("%d", &ls[i]);
    }
    printf ("\nENTER NUMBER TO SEARCH: ");
    scanf ("%d", &search);
    t=clock();
    ans = linear_search(ls, n, search);
    t = clock() - t;
    printf("\nTime taken: %f seconds.\n", ((double)t)/CLOCKS_PER_SEC);
    print_ans(ans, search);
    printf("\nBINARY SEARCH\n=============\n");
    printf ("\nENTER NUMBER OF ELEMENTS IN ARRAY: ");
    scanf ("%d", &n);
    int nls[n];
    printf ("\nENTER SORTED ARRAY ELEMENTS: ");
    for (i = 0; i < n; i++)
    {
      scanf ("%d", &nls[i]);
    }
    printf ("\nENTER NUMBER TO SEARCH: ");
    scanf ("%d", &search);
    t=clock();
    ans = binary_search(nls, n, search);
    t = clock() - t;
    printf("\nTime taken: %f seconds.\n", ((double)t)/CLOCKS_PER_SEC);
    print_ans(ans, search);
    printf("\n\nEXECUTE PROGRAM AGAIN (1 -> Y / 0 -> N): ");
    scanf("%d",&choice);
  }while (choice != 0);
  return 0;
}