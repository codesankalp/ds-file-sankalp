#include <stdio.h>
#include <string.h>

void naiveStringMatching(char *string, char *pattern)
{
    int M = strlen(pattern);
    int N = strlen(string);
    for (int i = 0; i <= N - M; i++)
    {
        int j;
        for (j = 0; j < M; j++)
            if (string[i + j] != pattern[j])
                break;
        if (j == M)
            printf("Pattern matches at index %d \n", i);
    }
}

int main()
{
    char str[100];
    char pattern[100];
    printf("Enter string:\t");
    scanf("%[^\n]%*c", str);
    printf("\nEnter pattern:\t");
    scanf("%[^\n]%*c", pattern);
    naiveStringMatching(str, pattern);
    return 0;
}
