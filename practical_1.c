/********************************
 @author: SANKALP
 roll no.: 03520902719
 *******************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

void print_arr(int arr[], int n){
    int i=0;
    printf("ARRAY = [ ");
    for(i; i<n-1; i++){
        printf("%d, ", arr[i]);
    }
    printf("%d ]\n", arr[n-1]);
}

void insertion_sort(int a[], int n){
    int i=0;
    for(i=1; i<n; i++){
        int curr = a[i], j= i-1;
        while (j>=0 && a[j]>curr){
            a[j+1] = a[j];
            j--;
            print_arr(a, n);
        }
        a[j+1] = curr;
    }
} 

void bubble_sort(int arr[], int n){
    int i,j,change;
    for (i=0; i<n-1; i++){
        change = 0;
        for (j=0; j<n-i-1; j++){
            if (arr[j]>arr[j+1]){
                int temp = arr[j];
                arr[j] = arr[j+1];
                arr[j+1] = temp;
                change = 1;
            }
            print_arr(arr, n);
        }
        if (change==0)
            break;
    }
}

void selection_sort(int arr[], int n){
    int i,j,min;
    for (i=0;i<n-1;i++){
        min = i;
        for (j=i+1;j<n;j++){
            if (arr[min]>arr[j])
                min = j;
            print_arr(arr, n);
        }
        int temp = arr[i];
        arr[i] = arr[min];
        arr[min] = temp;
    }
}

int main(){
    int n, i=0;
    clock_t t;
    printf("\nEnter Number of Elements in the Array (n): ");
    scanf("%d", &n);
    int arr[n];
    printf("\nEnter %d Array Elements: ", n);
    for (i;i<n;i++){
        scanf("%d", &arr[i]);
    }
    printf("\nACTUAL ARRAY\n============\n\n");
    print_arr(arr, n);
    
    t=clock();

    printf("\nINSERTION SORT\n==============\n\n");
    int insertion_sort_array[n];
    // Creating Copy of array.
    memcpy(insertion_sort_array, arr, n * sizeof(int));
    insertion_sort(insertion_sort_array, n);
    print_arr(insertion_sort_array, n);

    t = clock() - t;
    printf("\nTime taken: %f seconds.\n", ((double)t)/CLOCKS_PER_SEC);

    printf("\nBUBBLE SORT\n===========\n\n");
    int bubble_sort_array[n];
    memcpy(bubble_sort_array, arr, n * sizeof(int));
    bubble_sort(bubble_sort_array, n);
    print_arr(bubble_sort_array, n);

    t = clock() - t;
    printf("\nTime taken: %f seconds.\n", ((double)t)/CLOCKS_PER_SEC);

    printf("\nSELECTION SORT\n==============\n\n");
    int selection_sort_array[n];
    memcpy(selection_sort_array, arr, n * sizeof(int));
    selection_sort(selection_sort_array, n);
    print_arr(selection_sort_array, n);

    t = clock() - t;
    printf("\nTime taken: %f seconds.\n", ((double)t)/CLOCKS_PER_SEC);

    int choice = 1;
    while (choice){
        printf("\nPress 0 to exit: ");
        scanf("%d", &choice);
    }
    return 0;
}
