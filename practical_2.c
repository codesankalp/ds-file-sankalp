/********************************
 @author: SANKALP
 roll no.: 03520902719
 *******************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

void print_arr(int arr[], int n){
    int i=0;
    printf("ARRAY = [ ");
    for(i; i<n-1; i++){
        printf("%d, ", arr[i]);
    }
    printf("%d ]\n", arr[n-1]);
}

void merge(int arr[], int l, int m, int r){
    int nl = m - l + 1, nr = r-m, i, j, k;
    int left[nl], right[nr];
    for (i = 0; i < nl; i++)
        left[i] = arr[l + i];
    for (j = 0; j < nr; j++)
        right[j] = arr[m + 1 + j];
    i = 0;
    j = 0;
    k = l;
    while (i < nl && j < nr){
        if (left[i] <= right[j]){
            arr[k] = left[i];
            i++;
        }
        else{
            arr[k] = right[j];
            j++;
        }
        k++;
    }
    while (i < nl) {
        arr[k] = left[i];
        i++;k++;
    }
    while (j < nr) {
        arr[k] = right[j];
        j++;k++;
    }
}

void merge_sort(int a[], int low, int high){
    print_arr(a, high);
    if (low>=high)
        return;
    int mid = (low+high)/2;
    merge_sort(a, low, mid);
    merge_sort(a, mid+1, high);
    merge(a, low, mid, high);
}

int partition(int arr[], int start, int end){
    int pivot=arr[start], i=start, j=end;
    while (i<=j){
        if (arr[i]<=pivot){
            i++;continue;
        }
        if (arr[j]>pivot){
            j--;continue;
        }
        if (i<j){
            int temp = arr[i];
            arr[i] = arr[j];
            arr[j] = temp;
        }
    }
    arr[start] = arr[j];
    arr[j] = pivot;
    return j;  
}

void quick_sort(int arr[], int start, int end){
    print_arr(arr, end);
    if (start>=end)
        return;
    int index = partition(arr, start, end);
    quick_sort(arr, start, index-1);
    quick_sort(arr, index+1, end);
}

int main(){
    int n, i=0;
    clock_t t;
    printf("\nEnter Number of Elements in the Array (n): ");
    scanf("%d", &n);
    int arr[n];
    printf("\nEnter %d Array Elements: ", n);
    for (i;i<n;i++){
        scanf("%d", &arr[i]);
    }
    printf("\nACTUAL ARRAY\n============\n\n");
    print_arr(arr, n);
    
    t=clock();

    printf("\nMERGE SORT\n==========\n\n");
    int merge_sort_array[n];
    memcpy(merge_sort_array, arr, n * sizeof(int));
    merge_sort(merge_sort_array, 0, n-1);
    print_arr(merge_sort_array, n);
    t = clock() - t;
    printf("\nTime taken: %f seconds.\n", ((double)t)/CLOCKS_PER_SEC);
    
    printf("\nQUICK SORT\n==========\n\n");
    int quick_sort_array[n];
    memcpy(quick_sort_array, arr, n * sizeof(int));
    quick_sort(quick_sort_array, 0, n-1);
    print_arr(quick_sort_array, n);
    t = clock() - t;
    printf("\nTime taken: %f seconds.\n", ((double)t)/CLOCKS_PER_SEC);

    int choice = 1;
    while (choice){
        printf("\nPress 0 to exit: ");
        scanf("%d", &choice);
    }
    return 0;
}
