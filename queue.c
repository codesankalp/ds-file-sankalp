/*
@Author - Sankalp
Roll Number - 03520902719
Program - WAP TO IMPLEMENT A QUEUE USING LINKED LIST.
*/
#include <stdio.h> 
#include <stdlib.h> 

struct node { 
	int data; 
	struct node* next; 
}; 
 
struct Queue { 
	struct node *front, *rear; 
}; 
 
struct node* newNode(int k) 
{ 
	struct node* temp = (struct node*)malloc(sizeof(struct node)); 
	temp->data = k; 
	temp->next = NULL; 
	return temp; 
} 

struct Queue* createQueue() 
{ 
	struct Queue* q = (struct Queue*)malloc(sizeof(struct Queue)); 
	q->front = q->rear = NULL; 
	return q; 
} 

void print(struct node *head){
    if(head == NULL)
    {
        printf("NULL\n");
    }
    else
    {
        printf("%d ", head -> data);
        print(head->next);
    }
}

void enqueue(struct Queue* q, int k) 
{ 
	struct node* temp = newNode(k); 

	if (q->rear == NULL) { 
		q->front = q->rear = temp; 
		return; 
	} 

	q->rear->next = temp; 
	q->rear = temp; 
} 


void dequeue(struct Queue* q) 
{ 
	if (q->front == NULL) 
		return; 

	struct node* temp = q->front; 
    printf("Element deleted from queue is : %d\n", temp->data);
	q->front = q->front->next; 
 
	if (q->front == NULL) 
		q->rear = NULL; 

	free(temp); 
} 

void menu(){
    printf("\n===== MENU =====\n");
    printf("1. Enqueue\n");
    printf("2. Dequeue\n");
    printf("3. Display\n");
    printf("4. EXIT\n\n");
}

int main() 
{   
    int val=1,choice,num;
	struct Queue* q = createQueue();
	printf("\n====QUEUE USING LINKED LIST====\n\n");
	while (val){
        menu();
        printf("\nYour Choice: (1/2/3/4) ");
        scanf(" %d", &choice);
        switch (choice){
            case 1:
                printf("\nEnter value for enqueue in Queue: ");
                scanf("%d", &num);
                enqueue(q, num);
                break;
                
            case 2:
                dequeue(q);
                printf("\ndequeue successfull\n");
                break;
                
            case 3:
                print(q->front);
                break;
                
            case 4:
                val=0;
                break;
                
            default:
                printf("\nINVALID CHOICE !!!\n");
        }
    }
	return 0; 
} 
