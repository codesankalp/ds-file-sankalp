#include <stdio.h>
#include <string.h>

void rabin_karp_algo(char *string, char *pattern, int d, int q)
{
    int M = strlen(pattern);
    int N = strlen(string);
    int i, j;
    int p = 0;
    int t = 0;
    int h = 1;
    for (i = 0; i < M - 1; i++)
        h = (h * d) % q;
    for (i = 0; i < M; i++)
    {
        p = (d * p + pattern[i]) % q;
        t = (d * t + string[i]) % q;
    }
    for (i = 0; i <= N - M; i++)
    {
        if (p == t)
        {
            for (j = 0; j < M; j++)
            {
                if (string[i + j] != pattern[j])
                    break;
            }
            if (j == M)
                printf("Pattern found at index %d \n", i);
        }
        if (i < N - M)
        {
            t = (d * (t - string[i] * h) + string[i + M]) % q;
            if (t < 0)
                t = (t + q);
        }
    }
};

int main()
{
    char string[100];
    char pattern[100];
    int q;
    printf("Enter string:\t");
    scanf("%[^\n]%*c", string);
    printf("\nEnter pattern:\t");
    scanf("%[^\n]%*c", pattern);
    int d = 256;
    printf("Enter a prime number: ");
    scanf("%d", &q);
    rabin_karp_algo(string, pattern, d, q);
    return 0;
}
