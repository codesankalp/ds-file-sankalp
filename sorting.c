#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

void print_arr(int arr[], int n){
    int i=0;
    printf("ARRAY = [ ");
    for(i; i<n-1; i++){
        printf("%d, ", arr[i]);
    }
    printf("%d ]\n", arr[n-1]);
}

void insertion_sort(int a[], int n){
    int i=0;
    for(i=1; i<n; i++){
        print_arr(a, n);
        int curr = a[i], j= i-1;
        while (j>=0 && a[j]>curr){
            a[j+1] = a[j];
            j--;
        }
        a[j+1] = curr;
    }
} 

void merge(int arr[], int l, int m, int r){
    int nl = m - l + 1, nr = r-m, i, j, k;
    int left[nl], right[nr];
    for (i = 0; i < nl; i++)
        left[i] = arr[l + i];
    for (j = 0; j < nr; j++)
        right[j] = arr[m + 1 + j];
    i = 0;
    j = 0;
    k = l;
    while (i < nl && j < nr){
        if (left[i] <= right[j]){
            arr[k] = left[i];
            i++;
        }
        else{
            arr[k] = right[j];
            j++;
        }
        k++;
    }
    while (i < nl) {
        arr[k] = left[i];
        i++;k++;
    }
    while (j < nr) {
        arr[k] = right[j];
        j++;k++;
    }
}

void merge_sort(int a[], int low, int high){
    print_arr(a, high);
    if (low>=high)
        return;
    int mid = (low+high)/2;
    merge_sort(a, low, mid);
    merge_sort(a, mid+1, high);
    merge(a, low, mid, high);
}

int partition(int arr[], int start, int end){
    int pivot=arr[start], i=start, j=end;
    while (i<=j){
        if (arr[i]<=pivot){
            i++;continue;
        }
        if (arr[j]>pivot){
            j--;continue;
        }
        if (i<j){
            int temp = arr[i];
            arr[i] = arr[j];
            arr[j] = temp;
        }
    }
    arr[start] = arr[j];
    arr[j] = pivot;
    return j;  
}

void quick_sort(int arr[], int start, int end){
    if (start>=end)
        return;
    int index = partition(arr, start, end);
    quick_sort(arr, start, index-1);
    quick_sort(arr, index+1, end);
}

void bubble_sort(int arr[], int n){
    int i,j,change;
    for (i=0; i<n-1; i++){
        change = 0;
        for (j=0; j<n-i-1; j++){
            if (arr[j]>arr[j+1]){
                int temp = arr[j];
                arr[j] = arr[j+1];
                arr[j+1] = temp;
                change = 1;
            }
        }
        if (change==0)
            break;
    }
}

struct node{
    int data;
    struct node *next;
}*head[10];

void insert(int j, int data){
    if (head[j]==NULL){
        struct node *temp=(struct node*)malloc(sizeof(struct node));
        temp->data=data;
        temp->next=NULL;
        head[j]=temp;
    }
    else{
        struct node *temp = head[j];
        while (temp->next!=NULL){
            temp = temp->next;
        }
        temp->next=(struct node*)malloc(sizeof(struct node));
        temp=temp->next;
        temp->data=data;
        temp->next=NULL;
    }
}

int get_length(int j){
    struct node *temp;
    int count=0;
    temp=head[j];
    while(temp!=NULL){
        count++;
        temp=temp->next;
    }
    return count;
}

int k =0;

void ll_to_sorted_array(int j, int arr[], int n){
    int count = get_length(j);
    if (count){
        struct node *temp = head[j];
        int bucket_arr[count],i=0;
        while (temp!=NULL){
            bucket_arr[i] = temp->data;
            temp = temp->next;
            i++;
        }
        insertion_sort(bucket_arr, count);
        i=0;
        while (i<count && k<n){
            arr[k] = bucket_arr[i];
            i++;
            k++;
        }
    }
}

void bucket_sort(int arr[], int n){
    int max=arr[0],i,j,div,b=10;
    for(i=1;i<n;i++){
        if (arr[i]>max)
            max = arr[i];
    }
    double num = ((double)(max+1))/(double)(b);
    div = ceil(num);
    for (i=0;i<b;i++){
        head[i]=NULL;
    }
    for (i=0;i<n;i++){
        j = (floor)(arr[i]/div);
        insert(j, arr[i]);
    }
    for (i=0;i<b;i++){
        ll_to_sorted_array(i, arr, n);
    }   
}

void countSort(int arr[], int n, int p){ 
    int temp[n];
    int i, count[10] = { 0 }; 

    for (i = 0; i < n; i++) 
        count[(arr[i] / p) % 10]++; 
 
    for (i = 1; i < 10; i++) 
        count[i] += count[i - 1]; 
 
    for (i = n - 1; i >= 0; i--) { 
        temp[count[(arr[i] / p) % 10] - 1] = arr[i]; 
        count[(arr[i] / p) % 10]--; 
    } 

    for (i = 0; i < n; i++) 
        arr[i] = temp[i]; 
} 

void radix_sort(int arr[], int n){ 
    int max = arr[0], i; 
    for (int i = 1; i < n; i++) 
        if (arr[i] > max) 
            max = arr[i]; 
    for (i = 1; max /i > 0; i *= 10) 
        countSort(arr, n, i);
} 

void shell_sort(int arr[], int n){
    int i,k=n/2;
    while (k>0){
        for (i=k; i<n; i++){
            int curr = arr[i];
            int j = i;
            while (j>=k && arr[j-k]>curr){
                arr[j] = arr[j-k];
                j-=k;
            }
            arr[j]=curr;
        }
        k = k/2;
    }
}

void selection_sort(int arr[], int n){
    int i,j,min;
    for (i=0;i<n-1;i++){
        min = i;
        for (j=i+1;j<n;j++){
            if (arr[min]>arr[j])
                min = j;
        }
        int temp = arr[i];
        arr[i] = arr[min];
        arr[min] = temp;
    }
}

void heapify(int arr[], int n, int i){
    // print_arr(arr, n);
    int largest = i;
    int l = 2*i +1;
    int r = 2*i +2;
    if (l < n && arr[l]>arr[largest])
        largest = l;
    if (r < n && arr[r]>arr[largest])
        largest = r;
    if (largest!=i){
        int temp = arr[i];
        arr[i] = arr[largest];
        arr[largest] = temp;
        heapify(arr, n, largest);
    }
}

void heap_sort(int arr[], int n){
    int i;
    for(i= (n/2-1); i>=0; i--){
        heapify(arr, n, i);
    }
    for (i=n-1; i>=0; i--){
        int temp = arr[0];
        arr[0] = arr[i];
        arr[i] = temp;
        heapify(arr, i, 0);
    }
}

void exchange_sort(int arr[], int n){
    int i,j;
    for (i=0;i<n-1;i++){
        for (j=i+1; j<n; j++){
            if (arr[i]>arr[j]){
                int temp = arr[j];
                arr[j] = arr[i];
                arr[i] = temp;
            }
        }
    }
}

int main(){
    int n, i=0;
    printf("\nEnter Number of Elements in the Array (n): ");
    scanf("%d", &n);
    int arr[n];
    printf("\nEnter %d Array Elements: ", n);
    for (i;i<n;i++){
        scanf("%d", &arr[i]);
    }
    printf("\nACTUAL ARRAY\n============\n\n");
    print_arr(arr, n);
    
    printf("\nINSERTION SORT\n==============\n\n");
    int insertion_sort_array[n];
    // Creating Copy of array.
    memcpy(insertion_sort_array, arr, n * sizeof(int));
    insertion_sort(insertion_sort_array, n);
    print_arr(insertion_sort_array, n);
    
    printf("\nMERGE SORT\n==========\n\n");
    int merge_sort_array[n];
    memcpy(merge_sort_array, arr, n * sizeof(int));
    merge_sort(merge_sort_array, 0, n-1);
    print_arr(merge_sort_array, n);
    
    printf("\nQUICK SORT\n==========\n\n");
    int quick_sort_array[n];
    memcpy(quick_sort_array, arr, n * sizeof(int));
    quick_sort(quick_sort_array, 0, n-1);
    print_arr(quick_sort_array, n);
    
    printf("\nBUBBLE SORT\n===========\n\n");
    int bubble_sort_array[n];
    memcpy(bubble_sort_array, arr, n * sizeof(int));
    bubble_sort(bubble_sort_array, n);
    print_arr(bubble_sort_array, n);
    
    printf("\nBUCKET SORT\n===========\n\n");
    int bucket_sort_array[n];
    memcpy(bucket_sort_array, arr, n * sizeof(int));
    bucket_sort(bucket_sort_array, n);
    print_arr(bucket_sort_array, n);
    
    printf("\nRADIX SORT\n==========\n\n");
    int radix_sort_array[n];
    memcpy(radix_sort_array, arr, n * sizeof(int));
    radix_sort(radix_sort_array, n);
    print_arr(radix_sort_array, n);
    
    printf("\nSHELL SORT\n==========\n\n");
    int shell_sort_array[n];
    memcpy(shell_sort_array, arr, n * sizeof(int));
    shell_sort(shell_sort_array, n);
    print_arr(shell_sort_array, n);
    
    printf("\nSELECTION SORT\n==============\n\n");
    int selection_sort_array[n];
    memcpy(selection_sort_array, arr, n * sizeof(int));
    selection_sort(selection_sort_array, n);
    print_arr(selection_sort_array, n);
    
    printf("\nHEAP SORT\n=========\n\n");
    int heap_sort_array[n];
    memcpy(heap_sort_array, arr, n * sizeof(int));
    heap_sort(heap_sort_array, n);
    print_arr(heap_sort_array, n);
    
    printf("\nEXCHANGE SORT\n=============\n\n");
    int exchange_sort_array[n];
    memcpy(exchange_sort_array, arr, n * sizeof(int));
    exchange_sort(exchange_sort_array, n);
    print_arr(exchange_sort_array, n);
    int choice = 1;
    while (choice){
        printf("\nPress 0 to exit: ");
        scanf("%d", &choice);
    }
    return 0;
}