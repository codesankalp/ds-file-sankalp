/********************************
 Stack - Push, Pop, Traverse
 @author: SANKALP
 roll no.: 03520902719
 *******************************/
#include <stdio.h>
#include <stdlib.h>

struct node
{
    int data;
    struct node *next;
};

void menu(){
    printf("\n======= MENU =======\n");
    printf("1. Push In Stack.\n");
    printf("2. Pop In Stack\n");
    printf("3. Traverse Stack\n");
    printf("4. EXIT\n\n");
    printf("Enter the Choice (1/2/3/4): ");
}

void push(struct node **head, int val){
    struct node *new_node = (struct node *)malloc(sizeof(struct node));
    new_node->data = val;
    if (*head == NULL)
        new_node->next = NULL;
    else
        new_node->next = *head;
    *head = new_node;
}

void pop(struct node **head){
    if ((*head)==NULL)
    {
        printf("\nSTACK UNDERFLOW\n");
    }
    else
    {   
        struct node *ptr;
        ptr = *head;
        *head = (*head)->next;
        printf("Popped Element is %d\n", ptr->data);
        free(ptr);
    }
    
}

void traverse(struct node **head){
    struct node *ptr;
    if (*head==NULL)
    {
        printf("\nSTACK UNDERFLOW\n");
    }
    else
    {   
        ptr = *head;
        printf("<STACK ");
        while (ptr!=NULL){
            printf("%d ", ptr->data);
            ptr = ptr->next;
        }
        printf(">\n");
    }
}

int main(){
    int choice, val;
    struct node *head=NULL;
    while (choice){
        menu();
        scanf("%d",&choice);
        switch (choice)
        {
            case 1:
                printf("\nEnter value to push: ");
                scanf("%d", &val);
                push(&head, val);
                break;
            
            case 2:
                pop(&head);
                break;
            
            case 3:
                traverse(&head);
                break;
            
            case 4:
                choice=0;
                break;
            
            default:
                printf("Invalid Choice!");
                break;
        }
    }
    
    return 0;
}