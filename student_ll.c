#include<stdio.h>
#include<stdlib.h>

struct student {
    char name[50];
    int rollnumber; 
};

struct node {
    struct student st;
    struct node *next;
};

void menu(){
    printf("\n======= MENU =======\n");
    printf("1. Insert a Student.\n");
    printf("2. Delete Student With Roll Number.\n");
    printf("3. Reverse Students List.\n");
    printf("4. Display Students List\n");
    printf("5. EXIT\n\n");
    printf("Enter the Choice (1/2/3/4/5): ");
}

void insert(struct node **head){
    struct node *new_node = (struct node *)malloc(sizeof(struct node));
    printf("\nEnter Name Of Student (50): ");
    scanf("%s", &(new_node->st.name));
    printf("\nEnter Roll Number: ");
    scanf("%d", &(new_node->st.rollnumber));
    new_node->next = NULL;
    if (*head == NULL)
    {
        *head = new_node;
    }
    else
    {
        struct node *temp = *head;
        while (temp->next != NULL)
            temp = temp->next;
        temp->next = new_node;
    }
}

void display(struct node **head){
    struct node *temp = *head;
    if (*head==NULL){
        printf("\nNo Students Available.");
    }
    else{
        printf("\nSTUDENT\tROLL NUMBER\tNAME\n");
        int count=1;
        while (temp!=NULL){
            printf("%4d\t%5d\t\t%s\n",count, temp->st.rollnumber, temp->st.name);
            temp = temp->next;
            count++;
        } 
    }
}

void delete(struct node **head){
    if (*head ==NULL)
        printf("No Students Available To Delete.\n");
    else{
        int roll_number;
        printf("\nEnter Roll Number To Delete Student: ");
        scanf("%d", &roll_number);
        struct node* temp;
        struct node *prev;
        temp = *head;
        if (temp->st.rollnumber == roll_number){
            *head = temp->next;
            free(temp);
            return;
        }
        while(temp!=NULL){
            if (temp->st.rollnumber == roll_number){
                prev->next = temp->next;
                free(temp);
                return;
            }
            prev = temp;
            temp = temp->next;
        }
        printf("No Student available with roll number %d\n", roll_number);
    }
}

void reverse(struct node **head){
    if (*head == NULL)
        printf("No Students Available To Reverse.\n");
    else{
        struct node *curr = *head, *prev=NULL, *nxt=NULL;
        while (curr!=NULL){
            nxt = curr->next;
            curr->next = prev;
            prev = curr;
            curr = nxt;
        }
        *head = prev;
        printf("\nStudent List Reversed.\n");
    }
}

int main(){
    int choice;
    struct node* head=NULL;
    while (choice){
        menu();
        scanf("%d",&choice);
        switch (choice)
        {
            case 1:
                insert(&head);
                break;
            case 2:
                delete(&head);
                break;
            case 3:
                reverse(&head);
                break;
            case 4:
                display(&head);
                break;
            case 5:
                choice = 0;
                break;
            default:
                printf("Invalid Choice.\n");
                break;
        }
    }
    return 0;
}
